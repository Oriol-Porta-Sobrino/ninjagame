GIT: https://gitlab.com/Oriol-Porta-Sobrino/ninjagame.git

Nombre:
NinjaGame

Introduccion:
Juego simple de plataformas en el

Mecanicas:
- Salto
- Doble salto
- Cambiar arma
- Atacar

Elementos:
- Pinchos que hacen daño al jugador
- Enemigos que se mueven en horizontal y rebotan en paredes y en acantilados y pueden golpear al jugador, si el jugador golpea a los enemigos estos mueren
- Barra de vida
- Diferentes niveles

Controles:

- Space: Saltar
- A: Correr izquierda
- D: Correr derecha
- Q: Atacar
- 1: Equipar puño
- 2: Equipar espada (solo si la has conseguido en el mismo nivel)

Detalles tecnicos:
- Maquina de estados en enemigos y jugador
- Menu
- Sonidos
- Vairables y constantes globales para enemigos y jugador
- Estructuracion de codigo
- Reutilizacion de metodo
- Todo hecho con Terrains
- Uso de herencia tanto en maquinas de estado como en Armas