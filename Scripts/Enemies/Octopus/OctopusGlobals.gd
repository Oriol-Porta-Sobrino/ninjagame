extends Node

#Variable globals

var life : int = 1
var floor_speed : float = 1500.0
var gravity : float = 600.0
var falling_death_distance : float = 100
var direction : Vector2 = Vector2.RIGHT
var damage : int = 10
var attack_damage : int = 30
var damaged : bool = false
var invencible : bool = false
var invencible_time : float = 1
var jump : int = -150

#Sprites globals

var death_animation : String = "death"
var idle_animation : String = "idle"
var melee_attacking_animation : String = "melee_attack"
var walk_animation : String = "walk"

#States globals

var attack_state : StringName = "OctopusAttackState"
var damage_state : StringName = "OctopusDamageState"
var jump_state : StringName = "OctopusJumpState"
var idle_state : StringName = "OctopusIdleState"
var walk_state : StringName = "OctopusWalkState"
var falling_state : StringName = "OctopusFallingState"
var death_state : StringName = "OctopusDeathState"
