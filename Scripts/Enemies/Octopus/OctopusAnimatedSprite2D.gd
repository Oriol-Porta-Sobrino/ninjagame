extends AnimatedSprite2D

var hurt_box_collision : CollisionShape2D
var hit_box_collision : CollisionShape2D

func _ready():
	hurt_box_collision = $HurtboxArea/HurtboxCollision;
	hit_box_collision = $HitboxArea/HitboxCollision;

func _on_health_component_entity_dead():
	hurt_box_collision.set_deferred("disabled", true)
	hit_box_collision.set_deferred("disabled", true)
	play(OctopusGlobals.death_animation);
