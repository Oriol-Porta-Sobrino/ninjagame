class_name OctopusFallingState
extends State

func Physics_update(_delta):
	owner.velocity.y += OctopusGlobals.gravity * _delta
	if owner.is_on_floor():
		transitioned.emit(OctopusGlobals.idle_state)
	#owner.falling_death(owner)
	owner.move_and_slide()
