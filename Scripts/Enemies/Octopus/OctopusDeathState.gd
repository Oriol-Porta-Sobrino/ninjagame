class_name OctopusDeathState
extends State

@onready var death_sound : AudioStreamPlayer2D = get_node("%DeathSound");

func Enter():
	death_sound.play();
