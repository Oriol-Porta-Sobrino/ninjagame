class_name OctopusAttackState
extends State

var animation_player : AnimationPlayer

func _ready():
	animation_player = get_node("%OctopusAnimationPlayer");

func Enter():
	animation_player.play(OctopusGlobals.melee_attacking_animation);

func Update(_delta):
	if !animation_player.is_playing():
		transitioned.emit(OctopusGlobals.idle_state);
