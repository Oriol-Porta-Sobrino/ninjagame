extends Area2D

func _process(delta):
	for overlaping_body in get_overlapping_bodies():
		if overlaping_body.get_groups().has("player"):
			overlaping_body.health_component.damage_received(OctopusGlobals.attack_damage)
