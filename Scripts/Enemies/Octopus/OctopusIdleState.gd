class_name OctopusIdleState
extends State

var animation_sprites : AnimatedSprite2D

func Enter():
	animation_sprites = get_node("%OctopusAnimatedSprite2D")
	animation_sprites.play(OctopusGlobals.idle_animation)

func Physics_update(_delta):
	if !owner.is_on_floor():
		transitioned.emit(OctopusGlobals.falling_state)
	elif (!owner.wait):
		transitioned.emit(OctopusGlobals.walk_state)
