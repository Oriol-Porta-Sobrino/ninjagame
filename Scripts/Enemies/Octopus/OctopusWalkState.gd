class_name OctopusWalkState
extends State

var animation_sprites = AnimatedSprite2D

func _ready():
	animation_sprites = get_node("%OctopusAnimatedSprite2D");

func Physics_update(_delta):
	owner.velocity.x = 0
	if owner.attack_player_raycast.is_colliding() and owner.velocity.y == 0:
		transitioned.emit(OctopusGlobals.attack_state)
	elif owner.lower_tile_raycast.is_colliding() and !owner.upper_tile_raycast.is_colliding():
		owner.velocity.y = OctopusGlobals.jump
	elif !owner.is_on_floor():
		transitioned.emit(OctopusGlobals.falling_state)
	elif owner.is_on_wall():
		owner.vigilant_trun();
	elif !owner.tile_to_get_down_raycast.is_colliding() and !owner.actual_floor_raycast.is_colliding() and owner.actual_feet_floor_raycast.is_colliding():
		owner.vigilant_trun();
	owner.walk(_delta);
	owner.move_and_slide()
