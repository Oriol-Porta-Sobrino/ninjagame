extends CharacterBody2D

signal enemy_hit

@export var direction : Vector2 = Vector2.LEFT
@export var health_component : HealthComponent

@onready var animation_sprites : AnimatedSprite2D = $OctopusAnimatedSprite2D;
@onready var collision_shape : CollisionShape2D = $OctopusCollisionShape;
@onready var actual_floor_raycast : RayCast2D = $OctopusAnimatedSprite2D/RayCastToDetectFloorLeft;
@onready var actual_feet_floor_raycast : RayCast2D = $OctopusAnimatedSprite2D/RayCastToDetectFeetOnFloor;
@onready var upper_tile_raycast : RayCast2D = $OctopusAnimatedSprite2D/RayCastDetectUpperTile
@onready var lower_tile_raycast : RayCast2D = $OctopusAnimatedSprite2D/RayCastDetectLowerTile
@onready var tile_to_get_down_raycast : RayCast2D = $OctopusAnimatedSprite2D/RayCastDetectTileToGoDown
@onready var attack_player_raycast : RayCast2D = $OctopusAnimatedSprite2D/RayCastDetectPlayer
@onready var vigilant_turn_timer : Timer = $VigilantTurn;
@onready var state_machine : StateMachine = get_node("OctopusStateMachine");

var wait : bool = false;

func _ready():
	if direction == Vector2.RIGHT:
		flip_direction_to_right();

func flip_direction_to_right():
	direction = Vector2.RIGHT
	animation_sprites.scale.x *= -1;
	collision_shape.scale.x *= -1;
	collision_shape.position.x *= -1;

func flip_direction_to_left():
	direction = Vector2.LEFT
	animation_sprites.scale.x *= -1;
	collision_shape.scale.x *= -1;
	collision_shape.position.x *= -1;

func flip_direction():
	if direction == Vector2.LEFT:
		flip_direction_to_right();
	elif direction == Vector2.RIGHT:
		flip_direction_to_left();

func vigilant_trun():
	wait = true;
	vigilant_turn_timer.start();
	state_machine.on_child_transitioned(OctopusGlobals.idle_state);

func walk(_delta : float):
	if direction == Vector2.RIGHT:
		velocity.x += OctopusGlobals.floor_speed * _delta
	elif direction == Vector2.LEFT:
		velocity.x -= OctopusGlobals.floor_speed * _delta

func _on_damage_area_area_entered(area):
	if area.collision_layer&512 != 0:
		health_component.damage_received(area.do_damage())

func _on_health_component_entity_damaged(newHealth):
	pass;

func _on_health_component_entity_dead():
	enemy_hit.emit();
	state_machine.on_child_transitioned(OctopusGlobals.death_state);

func _on_vigilant_turn_timeout():
	flip_direction()
	position.x += direction.x
	move_and_slide()
	wait = false;
