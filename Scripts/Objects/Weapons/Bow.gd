class_name Bow
extends Weapon

func get_prefix_animation():
	return "bow";

func get_damage():
	return 5;

func get_weapon_name():
	return "bow";

func get_input():
	pass;
