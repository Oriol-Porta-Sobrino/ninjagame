class_name Sword
extends Weapon

func get_damage() -> int:
	return 10;

func get_weapon_name() -> String:
	return "sword";

func enemy_hit():
	queue_free();
