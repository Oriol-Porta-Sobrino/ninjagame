class_name Collectable
extends Area2D

var bob_height : float = 5.0
var bob_speed : float = 3.0

@onready var start_y_position : float = global_position.y
var time : float = 0.0

func _process(delta):
	# increase 't' over time.
	time += delta
	# create a sin wave that bobs up and down.
	var wave = (sin(time * bob_speed) + 1) / 2
	# apply that to the coin's Y position.
	global_position.y = start_y_position + (wave * bob_height)
