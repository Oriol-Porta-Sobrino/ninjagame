class_name SwordCollectable
extends Collectable

var sword : Weapon = Sword.new()
@onready var pickup_sound : AudioStreamPlayer2D = get_node("%PickupSound");

func _on_body_entered(body):
	if body.collision_layer&1 != 0:
		if (!pickup_sound.playing):
			pickup_sound.play();
		body.receive_weapon(sword)
		body.weapon_equiped = sword
		queue_free();
