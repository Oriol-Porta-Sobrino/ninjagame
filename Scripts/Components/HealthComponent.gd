class_name HealthComponent
extends Node2D

signal entity_damaged(newHealth);
signal entity_dead();

@export var MAX_HEALTH := 10;
@export var invencible_time : float = 0.0;
var invencible_timer : Timer;
var invencible : bool;
var health : int;

func _ready():
	health = MAX_HEALTH;
	invencible_timer = $PlayerInvencibleTimer;

func damage_received(damage : int):
	#TODO: Se podria lanzar desde aqui una señal de este mismo Script hacia el hud de cada uno de los enemigos para que todos tengan el hud de vida igual que el jugador
	if health <= 0:
		return;
	if not invencible:
		invencible = true
		invencible_timer.start(invencible_time);
		health -= damage
		if health <= 0:
			entity_dead.emit();
			return;
		entity_damaged.emit(health);

func _on_player_invencible_timer_timeout():
	invencible = false;
