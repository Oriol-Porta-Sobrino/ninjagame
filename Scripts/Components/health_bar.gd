class_name HealthBar
extends ProgressBar

@onready var show_damage_time : Timer = $ShowDamageTime
@onready var damage_bar : ProgressBar = $Damagebar

var health : int = 1 : set = _set_health

func _set_health(new_health):
	var previous_health = health
	health = min(max_value, new_health);
	value = health
	if health < 0:
		queue_free();
	if health < previous_health:
		show_damage_time.start();
	else:
		damage_bar.value = health

func init_health(_health):
	health = _health
	max_value = health
	value = health
	damage_bar.max_value = health
	damage_bar.value = health

func _on_show_damage_time_timeout():
	damage_bar.value = health
