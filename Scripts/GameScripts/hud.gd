extends CanvasLayer

var health_bar : ProgressBar;

func _ready():
	health_bar = get_node("HealthBar");
	health_bar.init_health(PlayerGlobals.start_life);

func _on_health_component_entity_damaged(newHealth):
	health_bar._set_health(newHealth);	
