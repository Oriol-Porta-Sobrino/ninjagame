extends Node2D

@export_file("*.tscn") var next_scene

func _on_area_2d_body_entered(body):
	if body.collision_layer&1 != 0:
		#get_tree().change_scene_to_file(next_scene)
		get_tree().call_deferred("change_scene_to_file", next_scene)
