class_name StateMachine
extends Node

@export var current_state : State
var character

var states : Dictionary = {}

# Called when the node enters the scene tree for the first time.
func _ready():
	for child in get_children():
		if child is State:
			# Add the state to the `Dictionary` using its `name`
			states[child.name] = child
			# Connect the state machine to the `transitioned` signal of all children
			child.transitioned.connect(on_child_transitioned);
		else:
			push_warning("State machine contains child which is not 'State'")
	# Start execution of the initial state
	current_state.Enter();
	
func on_child_transitioned(new_state_name: StringName) -> void:
	# Get the next state from the `Dictionary`
	var new_state = states.get(new_state_name)
	if new_state != null:
		if new_state != current_state:
			# Exit the current state
			current_state.Exit()
			# Enter the new state
			new_state.Enter()
			# Update the current state to the new one
			#Log.info(str(owner.name, ". Previous state: ", current_state.name, " | New state: ", new_state_name));			
			current_state = new_state
	else:
		push_warning("Called transition on a state that does not exist")

func _process(delta):
	current_state.Update(delta);
	
func _physics_process(delta):
	current_state.Physics_update(delta);
