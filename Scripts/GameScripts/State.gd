class_name State
extends Node

signal transitioned(new_game_state: StringName);
@export var character : CharacterBody2D

func Enter() -> void:
	pass;

func Exit() -> void:
	pass;
	
func Update(delta: float) -> void:
	pass;
		
func Physics_update(delta: float) -> void:
	pass
