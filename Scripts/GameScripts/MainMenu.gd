class_name MainMenu
extends Control

@onready var play_button = $MarginContainer/HBoxContainer/VBoxContainer/Play as Button
@onready var exit_button = $MarginContainer/HBoxContainer/VBoxContainer/Exit as Button
@onready var levels_button = $MarginContainer/HBoxContainer/VBoxContainer/Levels as Button
@onready var first_level = preload("res://Scenes/Levels/level_1.tscn") as PackedScene

func _ready():
	play_button.button_down.connect(on_play_pressed)
	levels_button.button_down.connect(on_levels_pressed)
	exit_button.button_down.connect(on_exit_pressed)

func on_play_pressed():
	get_tree().change_scene_to_packed(first_level);

func on_levels_pressed():
	pass # Replace with function body.

func on_exit_pressed():
	get_tree().quit();
