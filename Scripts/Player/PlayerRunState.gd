class_name PlayerRunState
extends State

var animation_sprites : AnimatedSprite2D

func _ready():
	animation_sprites = get_node("%PlayerAnimatedSprites");

func Physics_update(_delta):
	owner.can_change_weapon()
	owner.player_attack();
	owner.velocity.x = 0
	owner.floor_movement(_delta)
	if Input.is_action_just_pressed("Jump"):
		transitioned.emit(PlayerGlobals.jump_state);
	elif !owner.is_on_floor():
		transitioned.emit(PlayerGlobals.falling_state)
	elif owner.velocity.x == 0:
		transitioned.emit(PlayerGlobals.idle_state);
	owner.move_and_slide()
