class_name PlayerAttackState
extends State

var animation_player : AnimationPlayer
@onready var sword_attack_sound : AudioStreamPlayer2D = get_node("%SwordAttackSound");

func _ready():
	animation_player = get_node("%PlayerAnimationPlayer");

func Enter():
	sword_attack_sound.play();
	animation_player.play(owner.weapon_equiped.get_weapon_name() + "_" + PlayerGlobals.attack_animation);

func Update(_delta):
	if !animation_player.is_playing():
		transitioned.emit(PlayerGlobals.idle_state);
