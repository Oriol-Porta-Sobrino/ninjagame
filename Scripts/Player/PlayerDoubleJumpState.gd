class_name DoubleJumpState
extends State
 
var animation_sprites : AnimatedSprite2D
@onready var double_jump_sound : AudioStreamPlayer2D = get_node("%DoubleJumpSound");
 
func _ready():
	animation_sprites = get_node("%PlayerAnimatedSprites");

func Enter():
	double_jump_sound.play();
	owner.velocity.y = PlayerGlobals.double_jump_force
	owner.initial_jump_direction = owner.velocity.x
	get_node("%PlayerAnimatedSprites").play(owner.weapon_equiped.get_weapon_name() + "_" + PlayerGlobals.jumping_animation);
	owner.move_and_slide()
 
func Physics_update(_delta):
	owner.can_change_weapon()
	if owner.is_on_floor():
		transitioned.emit(PlayerGlobals.idle_state)
	owner.initial_jump_direction = owner.air_control(_delta);
	if owner.velocity.y > 0:
		transitioned.emit(PlayerGlobals.double_jump_falling_state)
	owner.velocity.y += PlayerGlobals.gravity * _delta
	owner.move_and_slide();
