class_name PlayerDoubleJumpFallingState
extends State

var animation_sprites : AnimatedSprite2D
 
func _ready():
	animation_sprites = get_node("%PlayerAnimatedSprites");

func Enter():
	get_node("%PlayerAnimatedSprites").play(owner.weapon_equiped.get_weapon_name() + "_" + PlayerGlobals.falling_animation);
	owner.initial_jump_direction = owner.velocity.x

func Exit():
	get_node("%PlayerAnimatedSprites").play(PlayerGlobals.landing_animation);

func Physics_update(_delta):
	owner.can_change_weapon()
	owner.velocity.y += PlayerGlobals.gravity * _delta
	owner.initial_jump_direction = owner.air_control(_delta);
	if owner.is_on_floor():
		transitioned.emit(PlayerGlobals.idle_state)
	owner.falling_death()
	owner.move_and_slide()
