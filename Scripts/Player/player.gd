class_name Player
extends CharacterBody2D

signal player_damaged(newHealth);
signal player_died();

@export var health_component : HealthComponent

@onready var animation_sprites : AnimatedSprite2D = get_node("%PlayerAnimatedSprites")
@onready var state_machine : StateMachine = get_node("PlayerStateMachine");

var life : int
var damaged : bool
var invencible : bool
var direction : Vector2
var initial_jump_direction : int = 0
var weapon_equiped : Weapon = PlayerGlobals.default_weapon
var weapons_in_inventory : Array = [PlayerGlobals.default_weapon]
var tools_equiped : Array = [PlayerGlobals.default_weapon]

func _ready():
	life = PlayerGlobals.start_life
	damaged = false
	invencible = false
	direction = PlayerGlobals.start_direction

func player_attack():
	if Input.is_action_just_pressed("Attack"):
		state_machine.on_child_transitioned(PlayerGlobals.attack_state);

func change_weapon(weapon_to_change : String):
	for weapon_in_inventory in weapons_in_inventory:
		if weapon_to_change == weapon_in_inventory.get_weapon_name():
			weapon_equiped = weapon_in_inventory;
			return true;

func can_change_weapon():
	if Input.is_action_just_pressed("Fist"):
		return change_weapon(PlayerGlobals.fist_name);
	elif Input.is_action_just_pressed("Sword"):
		return change_weapon(PlayerGlobals.sword_name);
	elif Input.is_action_just_pressed("Bow"):
		return change_weapon(PlayerGlobals.bow_name);

func receive_weapon(weapon : Weapon):
	weapons_in_inventory.append(weapon)

func falling_death():
	if position.y > PlayerGlobals.falling_death_distance:
		player_died.emit()

func air_control(_delta : float):
	if is_on_wall():
		initial_jump_direction = 0;
	if !Input.is_action_pressed("MoveRight") and !Input.is_action_pressed("MoveLeft") and initial_jump_direction != 0:
		velocity.x -= direction.x * 100 * _delta
	elif Input.is_action_pressed("MoveRight") and initial_jump_direction <= 0:
		flip_direction_to_right();
		if initial_jump_direction == 0:
			velocity.x = 0
		initial_jump_direction = 0
		velocity.x += PlayerGlobals.jump_speed * _delta;
	elif Input.is_action_pressed("MoveLeft") and initial_jump_direction >= 0:
		flip_direction_to_left();
		if initial_jump_direction == 0:
			velocity.x = 0
		initial_jump_direction = 0
		velocity.x -= PlayerGlobals.jump_speed * _delta
	return initial_jump_direction;

func flip_direction_to_right():
	direction = Vector2.RIGHT
	animation_sprites.scale.x = 1.5;

func flip_direction_to_left():
	direction = Vector2.LEFT
	animation_sprites.scale.x = -1.5;

func flip_direction():
	if direction == Vector2.LEFT:
		flip_direction_to_right();
	elif direction == Vector2.RIGHT:
		flip_direction_to_left();

func floor_movement(_delta : float):
	if Input.is_action_pressed("MoveRight"):
		flip_direction_to_right();
		animation_sprites.play(weapon_equiped.get_weapon_name() + "_" + PlayerGlobals.run_animation)
		velocity.x += PlayerGlobals.floor_speed * _delta;
	if Input.is_action_pressed("MoveLeft"):
		flip_direction_to_left();
		animation_sprites.play(weapon_equiped.get_weapon_name() + "_" + PlayerGlobals.run_animation)
		velocity.x -= PlayerGlobals.floor_speed * _delta

func die():
	queue_free()
	get_tree().call_deferred("reload_current_scene")

func _on_player_died():
	die();

func _on_health_component_entity_damaged(newHealth):
	state_machine.on_child_transitioned(PlayerGlobals.damage_state);

func _on_health_component_entity_dead():
	player_died.emit();

func _on_octopus_enemy_enemy_hit():
	if weapon_equiped is Sword:
		weapons_in_inventory.erase(weapon_equiped)
		weapon_equiped = weapons_in_inventory[0]
