class_name PlayerJumpState
extends State
 
var animation_sprites : AnimatedSprite2D
@onready var jump_sound : AudioStreamPlayer2D = get_node("%JumpSound");

func _ready():
	animation_sprites = get_node("%PlayerAnimatedSprites");

func Enter():
	jump_sound.play();
	owner.velocity.y += PlayerGlobals.jump_force
	owner.initial_jump_direction = owner.velocity.x
	get_node("%PlayerAnimatedSprites").play(owner.weapon_equiped.get_weapon_name() + "_" + PlayerGlobals.jumping_animation);
	owner.move_and_slide()

func Physics_update(_delta):
	owner.can_change_weapon()
	owner.velocity.y += PlayerGlobals.gravity * _delta
	owner.initial_jump_direction = owner.air_control(_delta);
	owner.move_and_slide()
	if Input.is_action_just_pressed("Jump"):
		transitioned.emit(PlayerGlobals.double_jump_state)
	if owner.velocity.y > 0:
		transitioned.emit(PlayerGlobals.falling_state)
	if owner.is_on_floor():
		transitioned.emit(PlayerGlobals.idle_state);
