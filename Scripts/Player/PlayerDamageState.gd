class_name PlayerDamageState
extends State

var animation_sprites : AnimatedSprite2D
var damage_timer : Timer
var invencible_timer : Timer
@onready var damage_sound : AudioStreamPlayer2D = get_node("%CharacterDamageSound");

func _ready():
	animation_sprites = get_node("%PlayerAnimatedSprites");
	damage_timer = $PlayerDamageTimer;
	invencible_timer = $PlayerInvencibleTimer;

func Enter():
	damage_sound.play();
	owner.damaged = true;
	owner.invencible = true
	var damage_jump_x
	if owner.direction == Vector2.RIGHT:
		damage_jump_x = -PlayerGlobals.damage_jump_x
	else:
		damage_jump_x = PlayerGlobals.damage_jump_x
	var damage_jump : Vector2 = Vector2(damage_jump_x, PlayerGlobals.damage_jump_y);
	owner.velocity = damage_jump
	damage_timer.start();
	invencible_timer.start(PlayerGlobals.invencible_time);
	animation_sprites.play(PlayerGlobals.damage_animation);

func Physics_update(_delta):
	owner.velocity.y += PlayerGlobals.gravity * _delta
	owner.move_and_slide()

func _on_damage_timer_timeout():
	owner.damaged = false;
	if owner.life <= 0:
		owner.player_died.emit()
	transitioned.emit(PlayerGlobals.idle_state);

func _on_invencible_timer_timeout():
	owner.invencible = false;
