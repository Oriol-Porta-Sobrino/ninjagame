extends Node

#Movement globals

var start_life : int = 100
var floor_speed : float = 6500.0
var jump_speed : float = 2500.0
var jump_force : float = -200.0
var run_jump_force : float = 50
var double_jump_force : float = -150.0
var falling_speed : float = 6500.0
var gravity : float = 600.0
var falling_death_distance : float = 100
var damage_jump_x : float = 100
var damage_jump_y : float = -100
var start_direction : Vector2 = Vector2.RIGHT
var invencible_time : float = 1

#Sprites globals

var default_weapon : Weapon = Fist.new()
var idle_animation : String = "idle"
var run_animation : String = "run"
var falling_animation : String = "falling"
var crouch_animation : String = "crouch"
var jumping_animation : String = "jumping"
var damage_animation : String = "damage"
var landing_animation : String = "landing"
var attack_animation : String = "attack"

#Player states

var attack_state : StringName = "PlayerAttackState"
var damage_state : StringName = "PlayerDamageState"
var double_jump_falling_state : StringName = "PlayerDoubleJumpFallingState"
var double_jump_state : StringName = "PlayerDoubleJumpState"
var jump_state : StringName = "PlayerJumpState"
var idle_state : StringName = "PlayerIdleState"
var run_state : StringName = "PlayerRunState"
var falling_state : StringName = "PlayerFallingState"

#Input globals

var walk_right : String = "Right";

#Weapon globals

var fist_name : String = "fist";
var sword_name : String = "sword";
var bow_name : String = "bow";
