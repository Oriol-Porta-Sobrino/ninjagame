class_name PlayerIdleState
extends State

func Physics_update(_delta):
	if owner.can_change_weapon():
		get_node("%PlayerAnimatedSprites").play(owner.weapon_equiped.get_weapon_name() + "_" + PlayerGlobals.idle_animation)
	if Input.is_action_pressed("MoveRight") or Input.is_action_pressed("MoveLeft"):
		transitioned.emit(PlayerGlobals.run_state);
	if !owner.is_on_floor():
		transitioned.emit(PlayerGlobals.falling_state)
	if Input.is_action_just_pressed("Jump"):
		transitioned.emit(PlayerGlobals.jump_state)
	if Input.is_action_just_pressed("Attack"):
		transitioned.emit(PlayerGlobals.attack_state)

func Enter() -> void:
	owner.velocity.x = 0
	get_node("%PlayerAnimatedSprites").play(owner.weapon_equiped.get_weapon_name() + "_" + PlayerGlobals.idle_animation)
