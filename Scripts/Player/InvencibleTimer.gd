extends Timer

var animation_sprites : AnimatedSprite2D
var blink_time = 0.2  # Duración de cada parpadeo
var blinking = 0.0

func _ready():
	animation_sprites = get_node("%PlayerAnimatedSprites");

func _process(delta):
	if is_stopped():
		return
	blinking += delta
	if blinking >= blink_time:
		blinking = 0.0
		animation_sprites.visible = !animation_sprites.visible;

func _on_timeout():
	animation_sprites.visible = true
