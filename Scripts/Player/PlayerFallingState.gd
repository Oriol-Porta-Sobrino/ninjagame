class_name PlayerFallingState
extends State

var animation_sprites : AnimatedSprite2D
 
func _ready():
	animation_sprites = get_node("%PlayerAnimatedSprites");

func Enter():
	animation_sprites.play(owner.weapon_equiped.get_weapon_name() + "_" + PlayerGlobals.falling_animation);
	owner.initial_jump_direction = owner.velocity.x

func Exit():
	animation_sprites.play(PlayerGlobals.landing_animation);

func Physics_update(_delta):
	owner.can_change_weapon()
	owner.velocity.y += PlayerGlobals.gravity * _delta
	owner.initial_jump_direction = owner.air_control(_delta);
	if Input.is_action_just_pressed("Jump"):
		transitioned.emit(PlayerGlobals.double_jump_state)
	if owner.is_on_floor():
		transitioned.emit(PlayerGlobals.idle_state)
	owner.falling_death()
	owner.move_and_slide()
